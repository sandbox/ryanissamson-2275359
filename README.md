UC Flexible Subscriptions
=======================

A module that allows administrators to manage Ubercart subscription renewals and files by role.

TODO
- Add better information for successful submission including affected users, changed dates, and added files.
- Use batch to process the subscriptions. Currently, there aren't too many but this will be helpful in the future.
